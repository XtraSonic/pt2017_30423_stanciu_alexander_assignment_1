/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynom;

/**
 *
 * @author Xtra Sonic
 */
public class NoLogarithmException extends Exception {

    /**
     * Creates a new instance of <code>NoLogarithmException</code> without
     * detail message.
     */
    public NoLogarithmException() {
    }

    /**
     * Constructs an instance of <code>NoLogarithmException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public NoLogarithmException(String msg) {
        super(msg);
    }
}
