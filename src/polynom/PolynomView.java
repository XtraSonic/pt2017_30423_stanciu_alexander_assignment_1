/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynom;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;
import javax.swing.*;
import javax.swing.event.DocumentListener;

/**
 *
 * @author Xtra Sonic
 */
public class PolynomView {
    
    
    private final PolynomModel polynomModel;

    private final JTextField inputA;
    private final JLabel inputALabel;
    private final JTextField inputB;
    private final JLabel inputBLabel;
    
    private final JTextField output;
    private final JLabel outputLabel;
    
    private final JButton addBtn;
    private final JButton subtractBtn;
    private final JButton multiplyBtn;
    private final JButton divideBtn;
    private final JButton derivateABtn;
    private final JButton derivateBBtn;
    private final JButton integrateABtn;
    private final JButton integrateBBtn;
    
    private final JFrame mainFrame;

    private static final String DEFAULT_INPUT="0";
    private static final String DEFAULT_OUTPUT="0";
    private static final int DEFAULT_GRID_WIDTH=2;
    
    
    PolynomView(PolynomModel model)
    {
        polynomModel = model;
        mainFrame = new JFrame();
        
        
        //input set
        inputA = new JTextField();
        inputB = new JTextField();
        inputA.setHorizontalAlignment(JTextField.CENTER);
        inputALabel = new JLabel("Polynom A: ");
        inputBLabel = new JLabel("Polynom B: ");
        inputB.setHorizontalAlignment(JTextField.CENTER);
        
        //output set
        output = new JTextField();
        output.setHorizontalAlignment(JTextField.CENTER);
        output.setEditable(false);
        outputLabel = new JLabel("Result: ");
        
        //set input and output values to default
        this.reset();
        
        //Buttons set
        addBtn = new JButton("add");
        subtractBtn = new JButton("subtract");
        multiplyBtn= new JButton("multiply");
        divideBtn= new JButton("divide");
        derivateABtn= new JButton("derivateA");
        derivateBBtn= new JButton("derivateB");
        integrateABtn= new JButton("integrateA");
        integrateBBtn= new JButton("integrateB");
        
        
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridBagLayout());
        
        GridBagConstraints constrains = new GridBagConstraints();
        constrains.fill = GridBagConstraints.HORIZONTAL;

        //Input A (lable + textfield)
        constrains.gridx = 0;
        constrains.gridy = 0;
        constrains.gridwidth = 1;
        mainPanel.add(inputALabel, constrains);
        constrains.gridwidth = 3;
        constrains.gridx = 1;
        mainPanel.add(inputA,constrains);
        
        //Input B (lable + textfield)
        constrains.gridx = 0;
        constrains.gridy = 1;
        constrains.gridwidth = 1;
        mainPanel.add(inputBLabel, constrains);
        constrains.gridwidth = 3;
        constrains.gridx = 1;
        mainPanel.add(inputB,constrains);
        
        //Output (lable + textfield)
        constrains.gridx = 0;
        constrains.gridy = 2;
        constrains.gridwidth = 1;
        mainPanel.add(outputLabel, constrains);
        constrains.gridwidth = 3;
        constrains.gridx = 1;
        mainPanel.add(output,constrains);
        
        //Addition + Subtraction
        constrains.gridx = 0;
        constrains.gridy = 3;
        constrains.gridwidth = DEFAULT_GRID_WIDTH;
        mainPanel.add(addBtn, constrains);
        constrains.gridx = DEFAULT_GRID_WIDTH;
        mainPanel.add(subtractBtn,constrains);
        
        
        //Multiplycation + Division
        constrains.gridx = 0;
        constrains.gridy = 4;
        constrains.gridwidth = DEFAULT_GRID_WIDTH;
        mainPanel.add(multiplyBtn, constrains);
        constrains.gridx = DEFAULT_GRID_WIDTH;
        mainPanel.add(divideBtn,constrains);
        
        //Derivation + Integration
        constrains.gridx = 0;
        constrains.gridy = 5;
        constrains.gridwidth = DEFAULT_GRID_WIDTH/2;
        mainPanel.add(derivateABtn, constrains);
        constrains.gridx = DEFAULT_GRID_WIDTH/2;
        mainPanel.add(derivateBBtn,constrains);
        constrains.gridx = DEFAULT_GRID_WIDTH;
        mainPanel.add(integrateABtn,constrains);
        constrains.gridx = DEFAULT_GRID_WIDTH+DEFAULT_GRID_WIDTH/2;
        mainPanel.add(integrateBBtn,constrains);
        
        //set main frame
        mainFrame.setContentPane(mainPanel);
        mainFrame.pack();
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setVisible(true);

    }
    
    public final void reset()
    {
        inputA.setText(DEFAULT_INPUT);
        inputB.setText(DEFAULT_INPUT);
        this.setOutput(DEFAULT_OUTPUT);    
    }
    
    public void setOutput(String out)
    {
        output.setText(out);
    }
    
    
    //Input -> getters + adding listiners
    public String getAInput(){
        return inputA.getText();
    }
    public String getBInput(){
        return inputB.getText();
    }
    public void addInputAListiner(DocumentListener l){
        inputA.getDocument().addDocumentListener(l);
    }
    public void addInputBListiner(DocumentListener l){
        inputB.getDocument().addDocumentListener(l);
    }
    
    //Buttons -> add listiners
    public void addAddBtnListiner(ActionListener l){
        addBtn.addActionListener(l);
    }
    public void addSubtractBtnListiner(ActionListener l){
        subtractBtn.addActionListener(l);
    }
    public void addMultiplyBtnListiner(ActionListener l){
        multiplyBtn.addActionListener(l);
    }
    public void addDivieBtnListiner(ActionListener l){
        divideBtn.addActionListener(l);
    }
    public void addDerivateABtnListiner(ActionListener l){
        derivateABtn.addActionListener(l);
    }
    public void addDerivateBBtnListiner(ActionListener l){
        derivateBBtn.addActionListener(l);
    }
    public void addIntegrateABtnListiner(ActionListener l){
        integrateABtn.addActionListener(l);
    }
    public void addIntegrateBBtnListiner(ActionListener l){
        integrateBBtn.addActionListener(l);
    }
    
    public String getInputA()
    {
        return this.inputA.getText();
    }
    public String getInputB()
    {
        return this.inputB.getText();
    }
    
}
