/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynom;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
//import javax.swing.event.DocumentEvent;
//import javax.swing.event.DocumentListener;

/**
 *
 * @author Xtra Sonic
 */
public class PolynomController {

    private final PolynomModel model;
    private final PolynomView view;

    PolynomController(PolynomModel model, PolynomView view) {
        this.model = model;
        this.view = view;

        //button listeners
        view.addAddBtnListiner(new AddBtnListener());
        view.addSubtractBtnListiner(new SubtractBtnListener());
        view.addMultiplyBtnListiner(new MultiplyBtnListener());
        view.addDivieBtnListiner(new DivideBtnListener());
        view.addDerivateABtnListiner(new DerivateABtnListener());
        view.addDerivateBBtnListiner(new DerivateBBtnListener());
        view.addIntegrateABtnListiner(new IntegrateABtnListener());
        view.addIntegrateBBtnListiner(new IntegrateBBtnListener());
        
        //text listener
        //view.addInputAListiner(new InputAListener());
        //no longer implementing text listeners for constant update of a and b
        //a and b will be read when a button is pressed
    }
    /*
    class InputAListener implements DocumentListener {


        @Override
        public void insertUpdate(DocumentEvent de) {
            model.setA(view.getInputA());
        }

        @Override
        public void removeUpdate(DocumentEvent de) {
            model.setA(view.getInputA());
        }

        @Override
        public void changedUpdate(DocumentEvent de) {
            model.setA(view.getInputA());
        }
    }
    
    class InputBListener implements DocumentListener {

        @Override
        public void insertUpdate(DocumentEvent de) {
            model.setB(view.getInputB());
        }

        @Override
        public void removeUpdate(DocumentEvent de) {
            model.setB(view.getInputB());
        }

        @Override
        public void changedUpdate(DocumentEvent de) {
            model.setB(view.getInputB());
        }
    }*/

    class AddBtnListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            try{
            model.readA(view.getAInput());
            model.readB(view.getBInput());
            model.add();
            view.setOutput(model.getResultString());
            }
            catch(IOException ioe)
            {
                view.setOutput(ioe.getMessage());
            }
        }
    }
    
    class SubtractBtnListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            try{
            model.readA(view.getAInput());
            model.readB(view.getBInput());
            model.subtract();
            view.setOutput(model.getResultString());
            }
            catch(IOException ioe)
            {
                view.setOutput(ioe.getMessage());
            }
        }
    }
    class MultiplyBtnListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            try{
            model.readA(view.getAInput());
            model.readB(view.getBInput());
            model.multiply();
            view.setOutput(model.getResultString());
            }
            catch(IOException ioe)
            {
                view.setOutput(ioe.getMessage());
            }
        }
    }
    class DivideBtnListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            try{
            model.readA(view.getAInput());
            model.readB(view.getBInput());
            model.divide();
            view.setOutput(model.getResultString());
            }
            catch(IOException ioe)
            {
                view.setOutput(ioe.getMessage());
            }
        }
    }
    class DerivateABtnListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            try{
            model.readA(view.getAInput());
            model.readB(view.getBInput());
            model.derivateA();
            view.setOutput(model.getResultString());
            }
            catch(IOException ioe)
            {
                view.setOutput(ioe.getMessage());
            }
        }
    }
    class DerivateBBtnListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            try{
            model.readA(view.getAInput());
            model.readB(view.getBInput());
            model.derivateB();
            view.setOutput(model.getResultString());
            }
            catch(IOException ioe)
            {
                view.setOutput(ioe.getMessage());
            }
        }
    }
    class IntegrateABtnListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            try{
            model.readA(view.getAInput());
            model.readB(view.getBInput());
            model.integrateA();
            view.setOutput(model.getResultString());
            }
            catch(IOException | NoLogarithmException e)
            {
                view.setOutput(e.getMessage());
            }
        }
    }
    class IntegrateBBtnListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent ae) {
            try{
            model.readA(view.getAInput());
            model.readB(view.getBInput());
            model.integrateB();
            view.setOutput(model.getResultString());
            }
            catch(IOException  | NoLogarithmException e)
            {
                view.setOutput(e.getMessage());
            }
        }
    }

}
