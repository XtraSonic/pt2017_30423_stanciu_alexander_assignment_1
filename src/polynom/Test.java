/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package polynom;

import java.util.Scanner;

/**
 *
 * @author XtraSonic
 */
public class Test {
    
    public static Polynom createPolynom(String s)
    {
        Polynom p = new Polynom();
        String coef=new String();
        String degree=new String();
        int i=0;
        while(i<s.length())
        {
            while(i<s.length()&&s.charAt(i)!='x' && s.charAt(i)!='X')
            {
                coef = coef.concat(Character.toString(s.charAt(i)));
                i++;
            }
            i+=2;
            while(i<s.length()&&s.charAt(i)!='+' && s.charAt(i)!='-')
            {
                degree = degree.concat(Character.toString(s.charAt(i)));
                i++;
            }
            p.addMonom(new Monom(Integer.parseInt(degree),Double.parseDouble(coef)));
            
            coef="";
            degree="";
        }
        return p;
    }
    
    public static void main(String[] args) {
        String text;
        PolynomView view = new PolynomView(new PolynomModel());
        PolynomModel model = new PolynomModel();
        PolynomController controller = new PolynomController(model, view);
        System.out.println("Input a polynom (ex: -3x^5+6x^3-2x^0)");
        Scanner scan = new Scanner(System.in); 
        text=scan.nextLine();
        Polynom p1 =Test.createPolynom(text);
        System.out.println(p1.toString());
        Polynom p2 =Test.createPolynom(text);
        System.out.println(p2.toString());
    }
}
