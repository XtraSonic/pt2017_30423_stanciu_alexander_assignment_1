/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynom;

/**
 *
 * @author XtraSonic
 */
public class Monom implements Comparable<Monom> {

    private final int degree;
    private double coefficient;

    Monom() {
        this(0, 0.0);
    }

    Monom(int degree, int coefficient) {
        this(degree, (double) coefficient);
    }

    Monom(int degree, double coefficient) {
        this.coefficient = coefficient;
        this.degree = degree;
    }

    public int getDegree() {
        return degree;
    }

    public double getCoefficient() {
        return coefficient;
    }

    public void addCoefficient(double i) {
        this.coefficient += i;
    }

    public Monom multiplyCoefficient(double multip) {
        return new Monom(this.degree, this.coefficient * multip);
    }

    public Monom divideCoefficient(double multip) {
        return new Monom(this.degree, this.coefficient / multip);
    }

    public Monom multiplyMonoms(Monom m) {
        Monom result;
        if(this.degree==0)
            result=m.multiplyCoefficient(this.coefficient);
        else if(m.degree==0)
            result=this.multiplyCoefficient(m.coefficient);
        else
            result=new Monom(this.degree + m.degree, this.coefficient * m.coefficient);
        return result;
    }

    public Monom divideMonoms(Monom m) {
        return new Monom(this.degree-m.degree, this.coefficient / m.coefficient );
    }
    public Monom negateMonom() {
        return new Monom(this.degree, -this.coefficient);
    }

    public Monom derivateMonom() {
        if (this.degree != 0) {
            return new Monom(this.degree - 1, this.coefficient * this.degree);
        } else {
            return new Monom(0, 0.0);
        }
    }
    
    public Monom integrateMonom() throws NoLogarithmException
    {
        if(this.degree!=-1)
        {
            return new Monom(this.degree+1,this.coefficient/(this.degree +1));
        }
        else
        {
            throw new NoLogarithmException("The result would have been a logarithm which can`t be converted to a polinom");
        }
    }

    @Override
    public int compareTo(Monom m) {
        final int BEFORE = -1;
        final int EQUAL = 0;
        final int AFTER = 1;

        if (this.degree > m.degree) {
            return BEFORE;
        } else if (this.degree == m.degree) {
            return EQUAL;
        } else {
            return AFTER;
        }
    }

    @Override
    public String toString() {
        String s = new String();
        if(this.coefficient>=0)
        {
            s = s.concat("+");
        }
        s = s.concat(Double.toString(this.coefficient));
        if (this.degree != 0) {
            s = s.concat("X^");
            s = s.concat(Integer.toString(this.degree));
           
        }

        return s;
    }

}
