/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynom;

import java.util.*;

/**
 *
 * @author XtraSonic
 */
public class Polynom {

    private ArrayList<Monom> monomList;

    Polynom(Polynom p) {
        this(p.monomList);
    }

    Polynom(Monom m) {
        this.monomList.add(m);
    }
    
    Polynom(ArrayList<Monom> monomList) {
        this.monomList = new ArrayList<>(monomList);
    }

    Polynom() {
        monomList = new ArrayList<>();
    }

    public void addMonom(Monom m) {
        boolean degreeExists = false;
        if(m.getCoefficient()==0)
            return;
        for (Monom i : monomList) {
            if (m.getDegree() == i.getDegree()) {
                degreeExists = true;
                i.addCoefficient(m.getCoefficient());
                if(i.getCoefficient()==0)
                    this.monomList.remove(i);
                break;
            }
        }
        if (!degreeExists) {
            monomList.add(m);
        }
    }

    public Polynom addPolynoms(Polynom p) {
        Polynom result = new Polynom(this.monomList);
        for (Monom m : p.monomList) {
            result.addMonom(m);
        }
        return result;
    }

    //subtracts p from this polinom (result=this-p)
    public Polynom subtractPolynoms(Polynom p) {
        Polynom result = new Polynom(this.monomList);
        for (Monom m : p.monomList) {
            result.addMonom(m.negateMonom());
        }
        return result;
    }

    public Polynom multiplyPolynoms(Polynom p) {
        Polynom result = new Polynom();
        for (Monom a : this.monomList) {
            for (Monom b : p.monomList) {
                result.addMonom(a.multiplyMonoms(b));
            }
        }
        return result;
    }

    
    //divides this polynom to p
    public Polynom dividePolynoms(Polynom p) {
        Polynom result = new Polynom();
        Polynom divided = new Polynom(this.monomList);
        Polynom divider = new Polynom(p.monomList);
        Monom aux;
        
        divided.sort();
        divider.sort();
        while(divided.monomList.get(0).getDegree()>=divider.monomList.get(0).getDegree())
        {
            aux=divided.monomList.get(0).divideMonoms(divider.monomList.get(0));
            result.addMonom(aux);
            divided=divided.subtractPolynoms(divider.multiplyPolynoms(new Polynom(aux)));
        }
        
        return result;
        
        //divided holds the reminder
    }

    public Polynom derivatePolynoms() {
        Polynom result = new Polynom();
        for (Monom m : this.monomList) {
            result.addMonom(m.derivateMonom());
        }
        return result;
    }

    public Polynom integratePolynom() throws NoLogarithmException {
        Polynom result = new Polynom();
        for (Monom m : this.monomList) {
            result.addMonom(m.integrateMonom());
        }
        return result;
    }

    public void sort() {
        Collections.sort(monomList, new Comparator<Monom>() {

            @Override
            public int compare(Monom o1, Monom o2) {
                return o1.compareTo(o2);
            }

        });
    }

    @Override
    public String toString() {
        String s = new String();
        for (Monom m : this.monomList) {
            s = s.concat(m.toString());
            
        }
        return s;
    }

}
