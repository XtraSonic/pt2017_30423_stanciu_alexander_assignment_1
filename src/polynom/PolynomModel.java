/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polynom;

import java.io.IOException;

/**
 *
 * @author Xtra Sonic
 */
class PolynomModel {
    private Polynom a;
    private Polynom b;
    private Polynom result;
    
    //seters
    public void setA(Polynom p){
        this.a=new Polynom(p);
    }
    public void setB(Polynom p){
        this.b=new Polynom(p);
    }
    
    //getter
    public String getResultString(){
        result.sort();
     return result.toString();
    }            
            
    public void readA(String s) throws IOException
    {
        try{
        this.setA(createPolynom(s));
        }
        catch (IOException ioe)
        {
            throw new IOException(ioe.getMessage() + " A");
        }
        catch (NumberFormatException nfe)
        {
            throw new IOException("Invalid Input A");
        }
    }
    public void readB(String s) throws IOException
    {
        try{
        this.setB(createPolynom(s));
        }
        catch (IOException ioe)
        {
            throw new IOException(ioe.getMessage() + " B");
        }
        catch (NumberFormatException nfe)
        {
            throw new IOException("Invalid Input B");
        }
    }
     
    public static Polynom createPolynom(String s) throws IOException
    {
        Polynom p = new Polynom();
        String coef=new String();
        String degree=new String();
        int i=0;
        while(i<s.length())
        {
            while(i<s.length()&&s.charAt(i)!='x' && s.charAt(i)!='X')
            {
                coef = coef.concat(Character.toString(s.charAt(i)));
                i++;
            }
            if(coef.equals(""))
            {
                coef="1";
            }
            if(i+1<s.length()&&s.charAt(i+1)=='^')
            {
                i+=2;
                if(s.charAt(i)=='-')
                {
                    i++;
                    degree= degree.concat("-");
                }
                while(i<s.length()&&s.charAt(i)!='+' && s.charAt(i)!='-')
                {
                    degree = degree.concat(Character.toString(s.charAt(i)));
                    i++;
                }
                
            }
            else
            {
                if((s.charAt(i-1)=='x'||s.charAt(i-1)=='X')&&!coef.equals("")&&!coef.equals("0"))
                {
                    degree = "1";
                }
                else
                {
                    degree = "0";
                }
                i++;
                
            }
            p.addMonom(new Monom(Integer.parseInt(degree),Double.parseDouble(coef)));
            coef="";
            degree="";
        }
        return p;
    }
    
    public void add(){
        result=a.addPolynoms(b);
    }
    public void subtract(){  
        result=a.subtractPolynoms(b);
    }
    public void multiply(){  
        result=a.multiplyPolynoms(b);
    }
    public void divide(){    
        result=a.dividePolynoms(b);
    }
    public void derivateA(){    
        result=a.derivatePolynoms();
    }
    public void derivateB(){     
        result=b.derivatePolynoms();
    }
    public void integrateA() throws NoLogarithmException{  
        result=a.integratePolynom();
    }
    public void integrateB() throws NoLogarithmException{      
        result=b.integratePolynom();
    }
          
}
